﻿// Christian Nilsson, AB4805
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Uppgift3
{
    class AVLTree : BinaryTree
    {
        /// <summary>
        /// Adds node to the tree, then checks if any of the parents to the added node has become unbalanced.
        /// </summary>
        /// <param name="node">Node to add</param>
        /// <param name="tree">Current node in tree</param>
        protected override void Add(TreeNode node, ref TreeNode tree)
        {
            if (node.Value < tree.Value)
            {
                TreeNode leftNode = tree.Left;

                if (leftNode == null) leftNode = node;
                else this.Add(node, ref leftNode);

                tree.Left = leftNode;
            }
            else if (node.Value > tree.Value)
            {
                TreeNode rightNode = tree.Right;

                if (rightNode == null) rightNode = node;
                else this.Add(node, ref rightNode);

                tree.Right = rightNode;
            }
            else throw new Exception("Value already exists.");

            // Now returning back through the tree to the root
            // Check height and balance, and rebalance if necessary

            BalanceNode(ref tree);
        }

        /// <summary>
        /// Delete a node from the AVL tree and rebalance it.
        /// </summary>
        /// <param name="value">Value to delete</param>
        /// <param name="tree">Current node</param>
        protected override void Delete(int value, ref TreeNode tree)
        {
            if (tree != null)
            {
                if (tree.Value == value)
                {
                    RemoveNode(ref tree);   // Found value, remove this node.
                }
                else if (value < tree.Value)
                {
                    TreeNode leftNode = tree.Left;
                    Delete(value, ref leftNode);     // Value is smaller than current node, delete in left side of tree
                    tree.Left = leftNode;
                }
                else if (value > tree.Value)
                {
                    TreeNode rightNode = tree.Right;
                    Delete(value, ref rightNode);    // Delete in right side of tree
                    tree.Right = rightNode;
                }
            }

            if (tree != null)
            {
                // Check balance of current node
                BalanceNode(ref tree);
            }
        }

        /// <summary>
        /// Checks the balance of a node and rebalances if balancefactor is not 1, 0, -1.
        /// </summary>
        /// <param name="tree">Node to check</param>
        private void BalanceNode(ref TreeNode tree)
        {
            int balanceFactor = GetBalanceFactor(tree);

            Debug.WriteLine("Current node: " + tree.Value + ", Node height: " + NodeHeight(tree) + ", Balance factor: " + balanceFactor);

            // Is current node unbalanced?
            if (balanceFactor > 1)
            {
                // left tree is higher
                if (NodeHeight(tree.Left.Left) < NodeHeight(tree.Left.Right))
                {
                    // Fall D i AVL2.pdf (left right)
                    TreeNode leftChild = tree.Left;
                    RotateLeft(ref leftChild);
                    tree.Left = leftChild;
                }

                // Fall B i AVL2.pdf (left left)
                RotateRight(ref tree);
            }
            else if (balanceFactor < -1)
            {
                // right tree is higher
                if (NodeHeight(tree.Right.Left) > NodeHeight(tree.Right.Right))
                {
                    // Fall C i AVL2.pdf (right left)
                    TreeNode rightChild = tree.Right;
                    RotateRight(ref rightChild);
                    tree.Right = rightChild;
                }

                // Fall A i AVL2.pdf (right right)
                RotateLeft(ref tree);
            }
        }

        /// <summary>
        /// Left rotate node: http://en.wikipedia.org/wiki/AVL_tree#Insertion (like in the Left Right Case & Right Right case)
        /// </summary>
        /// <param name="node">Node to rotate</param>
        private void RotateLeft(ref TreeNode node)
        {
            Debug.WriteLine("Left rotate: " + node.Value);
            TreeNode tempNode = node.Right;
            node.Right = tempNode.Left;
            tempNode.Left = node;
            node = tempNode;
        }

        /// <summary>
        /// Right rotate node: http://en.wikipedia.org/wiki/AVL_tree#Insertion (Left Left Case & Right Left Case)
        /// </summary>
        /// <param name="node">Node to rotate</param>
        private void RotateRight(ref TreeNode node)
        {
            Debug.WriteLine("Right rotate: " + node.Value);
            TreeNode tempNode = node.Left;
            node.Left = tempNode.Right;
            tempNode.Right = node;
            node = tempNode;
        }

        /// <summary>
        /// Get the balance factor of a node.
        /// </summary>
        /// <param name="node">Tree node</param>
        /// <returns>Balance factor</returns>
        private int GetBalanceFactor(TreeNode node)
        {
            return NodeHeight(node.Left) - NodeHeight(node.Right);
        }

        /// <summary>
        /// Get the height of a node
        /// </summary>
        /// <param name="node">Tree node</param>
        /// <returns>Height of node</returns>
        private int NodeHeight(TreeNode node)
        {
            if (node == null) return 0;
            else
            {
                int leftChildHeight = NodeHeight(node.Left);
                int rightChildHeight = NodeHeight(node.Right);

                if (leftChildHeight > rightChildHeight) return leftChildHeight + 1;
                else return rightChildHeight + 1;
            }
        }   
    }
}
