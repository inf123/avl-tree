﻿// Christian Nilsson, AB4805
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Uppgift3
{
    class BinaryTree
    {
        private TreeNode root = null;
        private int count = 0;

        /// <summary>
        /// Insert a value to the tree.
        /// </summary>
        /// <param name="value">Value to insert</param>
        /// <returns>Inserted node</returns>
        public TreeNode Insert(int value)
        {
            Debug.WriteLine("\nInsert value: " + value);
            TreeNode node = new TreeNode(value);
            try
            {
                if (root == null)
                    root = node;
                else
                    Add(node, ref root);

                count++;
                return node;
            }
            catch (Exception)
            {
                //Console.WriteLine("Value already exists");
                return null;
            }
        }

        /// <summary>
        /// Add a node to the tree
        /// </summary>
        /// <param name="node">Node to insert</param>
        /// <param name="tree">Tree to insert value in</param>
        protected virtual void Add(TreeNode node, ref TreeNode tree)
        {
            if (node.Value < tree.Value)
            {
                TreeNode leftNode = tree.Left;

                if (tree.Left == null) tree.Left = node;
                else this.Add(node, ref leftNode);
            }
            else if (node.Value > tree.Value)
            {
                TreeNode rightNode = tree.Right;

                if (tree.Right == null) tree.Right = node;
                else this.Add(node, ref rightNode);
            }
            else throw new Exception("Value already exists.");
        }

        /// <summary>
        /// Finds a value in the tree and returns its node
        /// </summary>
        /// <param name="value">Search value</param>
        /// <returns>Node with value or null if not found</returns>
        public TreeNode FindValue(int value)
        {
            return FindValue(value, this.root);
        }

        /// <summary>
        /// Finds a value in a sub tree and returns its node
        /// </summary>
        /// <param name="value">Search value</param>
        /// <param name="node">Sub tree to search</param>
        /// <returns>Node with value or null if not found</returns>
        private TreeNode FindValue(int value, TreeNode node)
        {
            if (node == null) return null;         
            else if (value < node.Value) return FindValue(value, node.Left);    // Search left side of tree
            else if (value > node.Value) return FindValue(value, node.Right);   // Search right side of tree
            else return node;   // Search value equals node.Value
        }

        /// <summary>
        /// Find a node with value and its parent
        /// </summary>
        /// <param name="value">Value to find</param>
        /// <param name="parent">Parent of node found</param>
        /// <returns>Node found</returns>
        private TreeNode FindParent(int value, ref TreeNode parent)
        {
            return FindParent(value, ref parent, ref this.root);
        }

        /// <summary>
        /// Find a node with value and its parent
        /// </summary>
        /// <param name="value">Value to find</param>
        /// <param name="parent">Parent of node found</param>
        /// <returns>Node found</returns>
        private TreeNode FindParent(int value, ref TreeNode parent, ref TreeNode node)
        {
            if (node == null) return null;
            else if (value < node.Value)
            {
                parent = node;
                TreeNode nextNode = node.Left;
                return FindParent(value, ref parent, ref nextNode);
            }
            else if (value > node.Value)
            {
                parent = node;
                TreeNode nextNode = node.Right;
                return FindParent(value, ref parent, ref nextNode);
            }
            else return node;
        }

        /// <summary>
        /// Find the left most node on the right side of a node.
        /// </summary>
        /// <param name="nodeToDelete">Node to search</param>
        /// <param name="parent">Parent of the node found</param>
        /// <returns>Node found</returns>
        public TreeNode LeftMostNodeOnRight(TreeNode nodeToDelete, ref TreeNode parent)
        {
            parent = nodeToDelete;
            nodeToDelete = nodeToDelete.Right;
            
            while (nodeToDelete.Left != null)
            {
                parent = nodeToDelete;
                nodeToDelete = nodeToDelete.Left;
            }

            return nodeToDelete;
        }


        /// <summary>
        /// Find a value and delete the node from the tree
        /// </summary>
        /// <param name="value">Value to delete</param>
        public void Delete(int value)
        {
            Debug.WriteLine("\n- Delete value: " + value);
            Delete(value, ref this.root);
            Debug.WriteLine("- Value deleted: " + value);
        }

        /// <summary>
        /// Find a value and delete the node from the tree
        /// </summary>
        /// <param name="value">Value to delete</param>
        /// <param name="tree">Current node</param>
        protected virtual void Delete(int value, ref TreeNode tree)
        {
            if (tree != null)
            {
                if (tree.Value == value)
                {
                    RemoveNode(ref tree);  // Found value, remove this node.
                }
                else if (value < tree.Value)
                {
                    TreeNode leftNode = tree.Left;
                    Delete(value, ref leftNode);     // Value is smaller than current node, delete on left side of tree
                    tree.Left = leftNode;
                }
                else if (value > tree.Value)
                {
                    TreeNode rightNode = tree.Right;
                    Delete(value, ref rightNode);    // Delete on right side of tree
                    tree.Right = rightNode;
                }
            }
        }

        /// <summary>
        /// Remove node from the tree
        /// </summary>
        /// <param name="nodeToDelete">Node to delete</param>
        protected void RemoveNode(ref TreeNode nodeToDelete)
        {
            if (nodeToDelete.Left == null && nodeToDelete.Right == null)
            {
                Debug.WriteLine("Node has no children.");
                nodeToDelete = null;
            }
            else if (nodeToDelete.Left == null && nodeToDelete.Right != null)
            {
                Debug.WriteLine("Node only has right child.");
                nodeToDelete = nodeToDelete.Right;
            }
            else if (nodeToDelete.Left != null && nodeToDelete.Right == null)
            {
                Debug.WriteLine("Node only has left child.");
                nodeToDelete = nodeToDelete.Left;
            }
            else if (nodeToDelete.Left != null && nodeToDelete.Right != null)
            {
                Debug.WriteLine("Node has two children.");
                TreeNode successorParent = null;
                TreeNode successor = LeftMostNodeOnRight(nodeToDelete, ref successorParent);
                TreeNode temp = new TreeNode(successor.Value);

                Debug.WriteLine("Successor: " + successor.Value);

                //if (successorParent.Left == successor) successorParent.Left = successor.Right;
                //else successorParent.Right = successor.Right;                  

                // Using Delete to remove successor from right subtree to rebalance it if this is an AVL tree.
                TreeNode rightNode = nodeToDelete.Right;
                Delete(successor.Value, ref rightNode);
                nodeToDelete.Right = rightNode;
                Debug.WriteLine("Successor deleted: " + successor.Value);

                nodeToDelete.Value = temp.Value;              
            }

            this.count--;
        }

        /// <summary>
        /// Inorder traversal. Returns a string with the values of the binary tree in ascending order.
        /// </summary>
        /// <returns>String of binary tree values in ascending order</returns>
        public string InorderTraversal() 
        {
            return InorderTraversal(root);
        }

        /// <summary>
        /// Inorder traversal of sub tree. Returns a string with the values of the binary tree in ascending order.
        /// </summary>
        /// <param name="node">Sub tree</param>
        /// <returns>String of binary tree values in ascending order</returns>
        public string InorderTraversal(TreeNode node)
        {
            string strOut = "";

            if (node != null)
            {
                strOut += InorderTraversal(node.Left);
                strOut += node.Value + " ";
                strOut += InorderTraversal(node.Right);
            }
            
            return strOut;
        }

        /// <summary>
        /// Create a string of current tree
        /// </summary>
        /// <returns>String of tree</returns>
        public string DrawTree()
        {
            return DrawNode(root);
        }

        /// <summary>
        /// Create a string of current tree
        /// </summary>
        /// <returns>String of tree</returns>
        private string DrawNode(TreeNode node)
        {
            if (node == null) return "empty";
            if ((node.Left == null) && (node.Right == null)) return "" + node.Value;
            if ((node.Left != null) && (node.Right == null)) return "" + node.Value + "(" + DrawNode(node.Left) + ", _)"; 
            if ((node.Right != null) && (node.Left == null)) return "" + node.Value + "(_, " + DrawNode(node.Right) + ")"; 
            return node.Value + "(" + DrawNode(node.Left) + ", " + DrawNode(node.Right) + ")";
        }
    }
}
