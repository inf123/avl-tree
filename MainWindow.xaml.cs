﻿// Christian Nilsson, AB4805
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;

namespace Uppgift3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //BinaryTree bt = new BinaryTree();
        AVLTree bt = new AVLTree();

        public MainWindow()
        {
            InitializeComponent();

            InitTree();
        }

        /// <summary>
        /// Init the tree, add some values and print it.
        /// </summary>
        private void InitTree()
        {
            bt.Insert(41);
            bt.Insert(25);
            bt.Insert(70);
            bt.Insert(10);
            bt.Insert(30);
            bt.Insert(60);
            bt.Insert(90);
            bt.Insert(115);
            
            txtOutput.Text += bt.DrawTree() + Environment.NewLine;
        }

        /// <summary>
        /// Add a node to the tree and print the tree
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            int inputNumber;

            if(int.TryParse(txtInput.Text, out inputNumber))
            {
                bt.Insert(inputNumber);

                txtOutput.Text += "Adding " + inputNumber + Environment.NewLine;
                txtOutput.Text += bt.DrawTree() + Environment.NewLine;
                scrollOutput.ScrollToBottom();
            }
            else MessageBox.Show("Error: Not a valid number");
            
        }

        /// <summary>
        /// Remove a node from the tree and print it.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            int inputNumber;

            if (int.TryParse(txtInput.Text, out inputNumber))
            {
                bt.Delete(inputNumber);

                txtOutput.Text += "Removing " + inputNumber + Environment.NewLine;
                txtOutput.Text += bt.DrawTree() + Environment.NewLine;
                scrollOutput.ScrollToBottom();
            }
            else MessageBox.Show("Error: Not a valid number");
        }

        /// <summary>
        /// Print inorder traversal of tree
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnInorderTraversal_Click(object sender, RoutedEventArgs e)
        {
            txtOutput.Text += bt.InorderTraversal() + Environment.NewLine;
        }
    }
}
