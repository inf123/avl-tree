﻿// Christian Nilsson, AB4805
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Uppgift3
{
    class TreeNode
    {
        private TreeNode left;
        private TreeNode right;
        private int value;

        public TreeNode(int value)
        {
            this.value = value;
            this.left = null;
            this.right = null;
        }
        
        /// <summary>
        /// Left child of this TreeNode
        /// </summary>
        public TreeNode Left
        {
            get { return this.left; }
            set { this.left = value; }
        }
        
        /// <summary>
        /// Right child of this TreeNode
        /// </summary>
        public TreeNode Right
        {
            get { return this.right; }
            set { this.right = value; }
        }

        /// <summary>
        /// Value of this TreeNode
        /// </summary>
        public int Value
        {
            get { return this.value; }
            set { this.value = value; }
        }
    }
}
